package com.chatly.example.user.dal.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lihuazeng
 */
@Getter
@Setter
public class User {
    private String id;
    private String name;
}
