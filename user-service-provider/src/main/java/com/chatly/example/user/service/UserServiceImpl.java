package com.chatly.example.user.service;

import com.chatly.example.user.api.UserService;
import com.chatly.example.user.api.dto.UserDTO;
import com.chatly.example.user.dal.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import java.util.UUID;

/**
 * 注册为dubbo服务,使用org.apache.dubbo.config.annotation.Service注解,非org.springframework.stereotype.Service注解
 * @author lihuazeng
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @SneakyThrows
    public UserDTO save(UserDTO userDto) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        BeanUtils.copyProperties(userDto, user);
        ObjectMapper objectMapper = new ObjectMapper();
        log.info("save user:{}", objectMapper.writeValueAsString(user));
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }
}
