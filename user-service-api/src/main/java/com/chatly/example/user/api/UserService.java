package com.chatly.example.user.api;

import com.chatly.example.user.api.dto.UserDTO;

/**
 * @author lihuazeng
 */
public interface UserService {
    UserDTO save(UserDTO userDto);
}
