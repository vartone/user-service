package com.chatly.example.user.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 服务间传输对象需实现Serializable接口
 * @author lihuazeng
 */
@Getter
@Setter
public class UserDTO implements Serializable {
    private String id;
    private String name;
}
